# HotBatBot #

This is the code that powers [@hotbatbot](https://twitter.com/hotbatbot) and [@batfunk](https://twitter.com/batfunk)

To replicate this you will need to get a client key and secret from twitter
https://apps.twitter.com/ will allow you to create an application to obtain
client key and secret.

then add them to the file `creds.json` where they are called `app_key` and
`app_secret`. If you have a single-user resource owner key and secret you can
also add them here:

    {
    "app_key": "",
    "app_secret": "",
    "accounts":[
    	{
    	"resource_owner_key":"",
    	"resource_owner_secret":"",
    	"id":,
    	"screen_name":""
    	}
    ]
    }

if you want to get resource owner keys via PIN-based authorisation you can somehow do that.

To run this needs the following python modules:

    oauthlib
    requests

`qrencode` is needed if you want to have a QR-code when authenticating an account.

