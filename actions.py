#!/usr/bin/env python3
# Actions performed by hotbatbot and batfunk. The user dict has specific end
# actions while the other functions are more general actions.

user = {
    "hotbatbot":retweet_tweets,
    "batfunk":copy_tweets
}

towers = {
    "#4815BBBK20C8":{
        "lon":9.162872,
        "lat":47.814101
    },
    "#4815BBBK20A0":{
        "lon":9.213793,
        "lat":47.80898
    },
    "#4815BBBK20BC":{
        "lon":9.283793,
        "lat":47.773867
    },
    "#3915BBBK03C7":{
        "lon":9.306057,
        "lat":47.733145
    }
}

def retrieve(user,last_id):
    """
    Retrieve sensorgnome tweets as user
    """
    if last_id == -1:
        status_code,url,sg_tweets = user.get_user_timeline(user_id=720428636738748417,count=200)
    else:
        status_code,url,sg_tweets = user.get_user_timeline(user_id=720428636738748417,max_id=last_id)
    print("%s Retrieve: %i" % (user.screen_name,status_code))
    if status_code != 200: print("Response: %s" % response)
    print("Retrieved %i tweets" % len(sg_tweets))
    return sg_tweets

def filter_tweets(tweets,filters):
    """
    Filter tweets according to filters as user
    """
    for filter_fun in filters: tweets = filter(filter_fun,tweets)
    tweets = sorted(tweets, key = lambda tweet: tweet["id"])
    print("%s found %i new tweets" % (user,len(tweets)))
    return tweets

def action_retweet(tweets,user):
    """
    Retweets the given tweets with account user
    """
    for tweet in tweets:
        status_code,url,response = user.retweet(tweet["id"])
        print("%s Retweet %i: %i" % (user.screen_name,tweet["id"],status_code))
        if status_code != 200: print("Response: %s" % response)

def action_copy(tweets,user):
    # not done
    for tweet in tweets:
        tower = tweet.split(" ")[0]
        status,url,resp = user.favorite(tweet["id"])
        print("%s Fav %i: %i" % (user.screen_name,tweet["id"],status))
        try: 
            status,url,resp = user.tweet(
                status=tweet.text,
                lat=towers[tower]["lat"],
                lon=towers[tower]["lon"]
            )
        except: status,url,resp = user.tweet(status=tweet.text)
        print("Tweet: %i" % status)


