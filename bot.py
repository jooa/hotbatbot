#!/usr/bin/env python3
import sys
import json
import time
import twitter
import filters
import actions

with open("creds.json","r") as cred_file:
    credentials = json.load(cred_file)
accounts = {
    account["screen_name"]: twitter.twitter(
        client_key = credentials["app_key"],
        client_secret = credentials["app_secret"],
        resource_owner_key = account["resource_owner_key"],
        resource_owner_secret = account["resource_owner_secret"],
        user_id = account["id"],
        screen_name = account["screen_name"]
    ) for account in credentials["accounts"]
}
print(accounts)

last_id = {"hotbatbot":-1,"batfunk":-1}

while True:
    for account in accounts:
        screen_name = account["screen_name"]
        tweets = actions.retrieve(account,last_id[screen_name])
        tweets = actions.filter_tweets(tweets,filters.functions[screen_name]])
        actions.user[screen_name](tweets,account)
        sys.stdout.flush()
        time.sleep(45)


