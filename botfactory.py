#!/usr/bin/python
# This script helps you with creating a bot.

import oauthlib.oauth1 as oauth
import subprocess
import requests
import shutil
import json
import os

def rc_parse(contentfile):
    """
    Read a config file and return a dict.
    Raise a SyntaxError if malformed.
    """
    with open(contentfile,'r') as f:
        content = f.read()
    content = content.strip().split('\n')
    line_n = 0
    return_dict = {}
    for line in content:
        line_n += 1
        line = line.split('#')[0].strip() # Remove comments
        if line == '': continue # skip empty
        line = line.split('=')
        if len(line) < 2: raise(SyntaxError('Line %i in file %s' % (line_n,contentfile)))
        return_dict[line[0].strip()] = line[1].strip()
    return return_dict

def qr_print(string):
    """
    Prints a UTF8 QR code that can be scanned by a phone.
    """
    command = ['qrencode']
    command.append('-t')
    command.append('UTF8')
    command.append(string)
    subprocess.call(command)


rcfiles = ['skel/token','skel/bot.conf']
rc = {}
for files in rcfiles: rc.update(rc_parse(files))

rc['name'] = input('Name: ')
os.mkdir('bots/%s' % rc['name'])
print('Copying files to "bots/%s"' % rc['name'])
shutil.copyfile('skel/bot.py','bots/%s/bot.py' % rc['name'])
shutil.copyfile('skel/bot.conf','bots/%s/bot.conf' % rc['name'])
shutil.copyfile('skel/token','bots/%s/token' % rc['name'])

print('Authenticating...')
# First bare-bones client instance
client = oauth.Client(
        rc['ConsumerKey'],
        client_secret = rc['ConsumerSecret']
)
url,headers,body = client.sign("https://api.twitter.com/oauth/request_token?oauth_callback=oob&x_auth_access_type=write",http_method="POST")
print('Obtaining Request Token...')
response = requests.post(url,headers=headers)
if not(response.ok): raise(Exception("%i: %s" % (response.status_code,response.text)))

print('--\n\n')
response_data = {value.split('=')[0]:value.split('=')[1] for value in response.text.split('&')}
# This data allows us to construct the URL for the end-user and the new client
client = oauth.Client(
         client_key            = rc['ConsumerKey'],
         client_secret         = rc['ConsumerSecret'],
         resource_owner_key    = response_data['oauth_token'],
         resource_owner_secret = response_data['oauth_token_secret']
)
url = "https://api.twitter.com/oauth/authorize?oauth_token=%s" % response_data['oauth_token']
qr_print(url)
print(url)
print('To continue, follow the URL, the instructions and enter the PIN below.')
PIN = input('PIN: ')

print("Obtaining Access Token...")
url,headers,body = client.sign("https://api.twitter.com/oauth/access_token?oauth_verifier=%s" % PIN,http_method="POST")
response = requests.post(url,headers=headers)
if not(response.ok): raise(Exception("%i: %s" % (response.status_code,response.text)))
response_data = {value.split('=')[0]:value.split('=')[1] for value in response.text.split('&')}
with open('bots/%s/token' % rc['name'],'a') as f: 
    f.write("TokenKey = %s" % response_data['oauth_token'])
    f.write("\n")
    f.write("TokenSecret = %s" % response_data['oauth_token_secret'])

with open('bots/%s/bot.conf' % rc['name'],'a') as f:
    f.write("ScreenName = %s" % response_data['screen_name'])
    f.write("\n")
    f.write("UserID = %s" % response_data['user_id'])

print("Your bot has been created. Please head over to it's directory and change stuff to your pleasing.")

