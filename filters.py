#!/usr/bin/env python3
# Filters to use with batfunk and hotbatbot. 
# conjunctions are created by additional elements in the list, disjunctions as
# part of a function.
# You can also just have one massive lambda in a list that deals with everything.

functions = {
    "batfunk": [lambda tweet: "#KNBat" in tweet],
    "hotbatbot": [
        lambda tweet: '#4815BBBK20C8' in tweet['text'] 
                   or '#3915BBBK03C7' in tweet['text']
                   or '#4815BBBK1892' in tweet['text']
                   or '#4815BBBK20BC' in tweet['text'],
        lambda tweet: tweet['retweeted'] == False
    ]
}


