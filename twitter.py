import urllib.parse
import json
import requests
import oauthlib.oauth1 as oauth

class twitter:
    """
    Twitter.
    """

    API_URL = "https://api.twitter.com/1.1/"

    def __init__(self, user_id, screen_name, resource_owner_key, 
            resource_owner_secret, client_key, client_secret):
        self.user_id = user_id
        self.screen_name = screen_name
        self.oauth_client = oauth.Client(
             client_key            = client_key,
             client_secret         = client_secret,
             resource_owner_key    = resource_owner_key,
             resource_owner_secret = resource_owner_secret
        )

    def __repr__(self):
        return "<Twitter User %s>" % self.user_id

    def __str__(self):
        return "<Twitter User %s>" % self.screen_name

    def _get(self, URL):
        """
        Signs a request to GET, GETs and return status_code,url,parsedjson-dict
        """
        signed = self.oauth_client.sign(URL,http_method='GET')
        r = requests.get(signed[0],headers=signed[1]) 
        print(r)
        return r.status_code, r.url, json.loads(r.text)

    def _post(self, URL):
        """
        Signs a request to POST, POSTs and return status_code,url,parsedjson-dict
        """
        signed = self.oauth_client.sign(URL,http_method='POST')
        r = requests.post(signed[0],headers=signed[1])
        print(r)
        return r.status_code, r.url, json.loads(r.text)

    def _compose_URL(self,URL_base,options):
        """
        Add options to a URL, formatting options according to RFC1738.
        """
        optpairs = [urllib.parse.quote(key) + "=" +urllib.parse.quote(options[key]) for key in options]
        return URL_base + "?" + "&".join(optpairs)

    def get_user_timeline(self,screen_name=None,user_id=-1,
            count=100,max_id=-1,since_id=-1):
        """
        GET a user timeline by GET statuses/user_timeline
        Options: screen_name, user_id, count,
                 max_id, since_id
        pass either user_id or screen_name
        """
        resource_URL = self.API_URL + "statuses/user_timeline.json"

        if screen_name=None and user_id=-1:
            raise(ValueError("Neither screen_name nor user_id passed"))
        elif screen_name != None and user_id != -1:
            raise(ValueError("Both screen_name and user_id passed"))

        options = {"count": str(count)}
        if screen_name != None: options["screen_name"] = str(screen_name)
        else: options["user_id"] = str(user_id)
        if max_id != -1: options["max_id"] = str(max_id)
        if since_id != -1: options["max_id"] = str(since_id)

        URL = self._compose_URL(resource_URL,options)
        return self._get(URL)

    def favorite(self,tweet_id):
        """
        POST to favorite a tweet with tweet_id
        """
        resource_URL = self.API_URL + "favorites/create.json"
        options = {"id":tweet_id}
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)

    def unfavorite(self,tweet_id):
        """
        POST to unfavorite a tweet with tweet_id
        """
        resource_URL = self.API_URL + "favorites/destroy.json"
        options = {"id":tweet_id}
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)

    def retweet(self,tweet_id):
        """
        POST to retweet a tweet with tweet_id
        """
        resource_URL = self.API_URL + "statuses/retweet/" + str(tweet_id) + ".json"
        options = {"id":tweet_id}
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)

    def unretweet(self,tweet_id):
        """
        POST to unretweet a tweet with tweet_id
        """
        resource_URL = self.API_URL + "statuses/unretweet/" + str(tweet_id) + ".json"
        options = {"id":tweet_id}
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)

    def tweet(self, status, lat=None, lon=None):
        """
        POST a tweet (update status) by POST statuses/update
        """
        resource_URL = self.API_URL + "statuses/update.json"
        options = {"status": status}
        if lat != None and lon != None:
            options["lat"]  = str(lat)
            options["long"] = str(lon)
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)

    def untweet(self, status, lat=None, lon=None):
        """
        POST to delete a tweet
        """
        resource_URL = self.API_URL + "statuses/destroy/" + tweet_id + ".json"
        options = {"id": tweet_id}
        URL = self._compose_URL(resource_URL,options)
        return self._post(URL)
            
